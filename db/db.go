// Package db contains interface and struct types to be implemented by storage backends.
package db

import (
	"time"

	"emperror.dev/errors"
	"github.com/google/uuid"
)

// DB is a database to store URLs, their expiration, and users.
type DB interface {
	// URL returns the full URL of the given short link
	URL(short string) (u URL, err error)

	// RandomURL generates a random short URL
	RandomURL(userID uuid.UUID, url string) (u URL, err error)
	// NamedURL creates a short URL with the given name
	NamedURL(userID uuid.UUID, short, url string) (u URL, err error)

	DeleteURL(short string) (err error)

	// User returns the user with the given UUID.
	User(id uuid.UUID) (u User, err error)
	Username(name string) (u User, err error)
	// CreateUser creates a user with the given username
	CreateUser(name string) (u User, token string, err error)
	// ValidateToken validates the given token
	ValidateToken(token string) (u *User, valid bool, err error)
	// RegenerateToken generates a new token
	RegenerateToken(id uuid.UUID) (token string, err error)
	DeleteUser(id uuid.UUID) (err error)

	AllLinks() ([]URL, error)
	UserLinks(id uuid.UUID) ([]URL, error)
}

// URL is a single short URL.
type URL struct {
	ID uuid.UUID
	// The short URL
	Short string
	// The full URL to redirect to
	URL string
	// The user that created this short URL
	User User
	// The time this URL expires and is removed from the database, nil if it doesn't expire.
	Expires *time.Time
}

// User is a user.
type User struct {
	ID       uuid.UUID
	Username string
}

const (
	// ErrAlreadyExists is returned when a URL with the given name already exists.
	ErrAlreadyExists = errors.Sentinel("url with this name already exists")
	// ErrNotFound is returned when a URL with the given name doesn't exist.
	ErrNotFound = errors.Sentinel("url with this name doesn't exist")

	// ErrUserNotFound is returned when the given user is not found.
	ErrUserNotFound = errors.Sentinel("user not found")
	// ErrInvalidUsername is returned when the given username isn't valid.
	ErrInvalidUsername = errors.Sentinel("invalid username")
)
