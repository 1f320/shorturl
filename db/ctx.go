package db

import "context"

type contextKey int

const (
	contextKeyUser contextKey = iota
)

// NewUserContext ...
func NewUserContext(ctx context.Context, user *User) context.Context {
	return context.WithValue(ctx, contextKeyUser, user)
}

// UserFromContext ...
func UserFromContext(ctx context.Context) (*User, bool) {
	val := ctx.Value(contextKeyUser)

	u, ok := val.(*User)
	return u, ok
}
