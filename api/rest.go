package api

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"

	"emperror.dev/errors"
)

// Errors returned by Request
const (
	ErrBadRequest    = errors.Sentinel("400 bad request")
	ErrUnauthorized  = errors.Sentinel("401 unauthorized")
	ErrNotFound      = errors.Sentinel("404 not found")
	ErrAlreadyExists = errors.Sentinel("409 resource already exists")
	ErrUnprocessable = errors.Sentinel("422 unprocessable entity")
	ErrUnavailable   = errors.Sentinel("503 service unavailable")
)

type apiError int

func (e apiError) Error() string {
	return fmt.Sprintf("%v %v", int(e), http.StatusText(int(e)))
}

const (
	EndpointLinks = "/links/"
	EndpointUsers = "/users/"
	EndpointInfo  = "/info.json"
)

// Request makes a request returning a JSON body.
func (s *Client) Request(method, endpoint string, opts ...RequestOption) (response []byte, err error) {
	req, err := http.NewRequest(method, s.BaseURL+endpoint, nil)
	if err != nil {
		return
	}

	err = s.applyOpts(req, opts)
	if err != nil {
		return nil, err
	}

	ctx, cancel := context.WithTimeout(context.Background(), s.Timeout)
	defer cancel()

	err = s.rate.Wait(ctx)
	if err != nil {
		return
	}

	resp, err := s.Client.Do(req)
	if err != nil {
		return
	}
	defer resp.Body.Close()

	response, err = io.ReadAll(resp.Body)
	if err != nil {
		return
	}

	switch resp.StatusCode {
	case http.StatusOK, http.StatusNoContent, http.StatusCreated:
	case http.StatusBadRequest:
		return nil, ErrBadRequest
	case http.StatusUnauthorized:
		return nil, ErrUnauthorized
	case http.StatusNotFound:
		return nil, ErrNotFound
	case http.StatusConflict:
		return nil, ErrAlreadyExists
	case http.StatusUnprocessableEntity:
		return nil, ErrUnprocessable
	case http.StatusServiceUnavailable:
		return nil, ErrUnavailable
	default:
		return nil, apiError(resp.StatusCode)
	}

	return response, err
}

// RequestJSON makes a request returning a JSON body.
func (s *Client) RequestJSON(v interface{}, method, endpoint string, opts ...RequestOption) error {
	resp, err := s.Request(method, endpoint, opts...)
	if err != nil {
		return err
	}

	if v == nil {
		return nil
	}

	return json.Unmarshal(resp, v)
}

// applyOpts applies all options to the given request and returns the last error returned by an option.
func (s *Client) applyOpts(r *http.Request, opts []RequestOption) (err error) {
	// apply global options
	for _, opt := range s.RequestOptions {
		err = opt(r)
	}

	// apply local options
	for _, opt := range opts {
		err = opt(r)
	}
	return
}
