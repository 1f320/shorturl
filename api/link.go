package api

import (
	"time"

	"github.com/google/uuid"
)

// Link is a link.
type Link struct {
	ID      uuid.UUID  `json:"id"`
	Short   string     `json:"short"`
	URL     string     `json:"url"`
	Expires *time.Time `json:"expires,omitempty"`
}

// Links returns all links.
func (s *Client) Links() (links []Link, err error) {
	err = s.RequestJSON(&links, "GET", EndpointLinks)
	if err != nil {
		return nil, err
	}
	return links, nil
}

// MyLinks returns the links created by the user whose token is being used.
func (s *Client) MyLinks() (links []Link, err error) {
	return s.UserLinks("@me")
}

// UserLinks returns this user's links.
func (s *Client) UserLinks(username string) (links []Link, err error) {
	err = s.RequestJSON(&links, "GET", EndpointUsers+username)
	if err != nil {
		return nil, err
	}
	return links, nil
}

// CreateLinkData is the data passed to CreateLinkComplex.
type CreateLinkData struct {
	// The short name for this link. Randomly generated if empty.
	Name string `json:"name,omitempty"`
	// The URL this link points to.
	URL string `json:"url"`
	// The expiry for this link. Nil if it doesn't expire.
	Expires *time.Time `json:"expires,omitempty"`
}

// Expires returns a *time.Time
func Expires(toAdd time.Duration) *time.Time {
	t := time.Now().UTC().Add(toAdd)
	return &t
}

// CreateLink creates a short URL to the given link.
func (s *Client) CreateLink(url string) (link Link, err error) {
	return s.CreateLinkComplex(CreateLinkData{URL: url})
}

// CreateLinkComplex creates a short URL to the given link.
func (s *Client) CreateLinkComplex(data CreateLinkData) (link Link, err error) {
	err = s.RequestJSON(&link, "POST", EndpointLinks, WithJSONBody(data))
	return link, err
}

// LinkInfo returns a link by short URL.
func (s *Client) LinkInfo(short string) (link Link, err error) {
	err = s.RequestJSON(&link, "GET", EndpointLinks+short+EndpointInfo)
	return link, err
}
