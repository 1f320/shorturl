// Package api contains types and methods for interacting with a shorturl server.
package api

import (
	"net/http"
	"strings"
	"time"

	"golang.org/x/time/rate"
)

// Client is a shorturl API client.
type Client struct {
	token string

	BaseURL string
	rate    *rate.Limiter

	Client *http.Client

	// The user agent used by requests.
	UserAgent string

	// The default timeout for requests.
	Timeout time.Duration

	// Debug is called for debug logs. Default no-op.
	Debug func(tmpl string, v ...interface{})

	// RequestOptions are applied to every outgoing request.
	RequestOptions []RequestOption
}

// NewClient returns a new Client.
func NewClient(baseURL, token string, requestsPerMinute int) *Client {
	limit := rate.Limit(requestsPerMinute)
	if requestsPerMinute == 0 {
		limit = rate.Inf
	}

	c := &Client{
		token:     token,
		BaseURL:   strings.Trim(baseURL, "/"),
		rate:      rate.NewLimiter(limit, requestsPerMinute),
		Timeout:   20 * time.Second,
		Debug:     func(string, ...interface{}) {},
		Client:    &http.Client{Timeout: 20 * time.Second},
		UserAgent: "go/shorturl-client",
	}

	fn := func(req *http.Request) error {
		req.Header.Set("Authorization", c.token)
		return nil
	}

	c.RequestOptions = append(c.RequestOptions, fn)

	return c
}
