package main

import (
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/u1f320/shorturl/db"
)

func (s *server) deleteLink(w http.ResponseWriter, r *http.Request) {
	link := chi.URLParam(r, "link")

	url, err := s.db.URL(link)
	if err != nil {
		if err == db.ErrNotFound {
			jsonError(w, r, http.StatusNotFound, "link not found")
			return
		}

		s.log.Errorf("Error fetching link: %v", err)
		jsonError(w, r, http.StatusInternalServerError, "Internal server error")
		return
	}

	u, ok := db.UserFromContext(r.Context())
	if !ok {
		jsonError(w, r, http.StatusInternalServerError, "Internal server error")
		return
	}

	if url.User.ID != u.ID {
		jsonError(w, r, http.StatusUnauthorized, "You can only delete your own links")
		return
	}

	err = s.db.DeleteURL(url.Short)
	if err != nil {
		s.log.Errorf("Couldn't delete link: %v", err)
		jsonError(w, r, http.StatusInternalServerError, "Internal server error")
		return
	}

	w.WriteHeader(http.StatusNoContent)
}
