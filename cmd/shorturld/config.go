package main

// Constants for the `store_type` field in the config
const (
	PostgresStore = "postgres"
	MemoryStore   = "memory"
)
