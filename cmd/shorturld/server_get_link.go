package main

import (
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/render"
	"github.com/u1f320/shorturl/db"
)

func (s *server) getLink(w http.ResponseWriter, r *http.Request) {
	link := chi.URLParam(r, "link")

	url, err := s.db.URL(link)
	if err != nil {
		if err == db.ErrNotFound {
			jsonError(w, r, http.StatusNotFound, "No short URL with that link found")
			return
		}

		s.log.Errorf("Error getting link: %v", err)
		jsonError(w, r, http.StatusInternalServerError, "Internal server error")
		return
	}

	http.Redirect(w, r, url.URL, http.StatusTemporaryRedirect)
	return
}

func (s *server) getLinkJSON(w http.ResponseWriter, r *http.Request) {
	link := chi.URLParam(r, "link")

	url, err := s.db.URL(link)
	if err != nil {
		if err == db.ErrNotFound {
			jsonError(w, r, http.StatusNotFound, "No short URL with that link found")
			return
		}

		s.log.Errorf("Error getting link: %v", err)
		jsonError(w, r, http.StatusInternalServerError, "Internal server error")
		return
	}

	render.JSON(w, r, toLinkData(url))
}
