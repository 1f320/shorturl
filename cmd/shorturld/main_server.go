package main

import (
	"net/http"

	"github.com/u1f320/shorturl/db"
	"go.uber.org/zap"
)

func serverMain(db db.DB, log *zap.SugaredLogger) {
	s := newServer(db, log)

	ch := make(chan error, 1)
	go func() {
		ch <- http.ListenAndServe(":"+port, s.mux)
	}()

	log.Infof("Server is now running on port %v", port)

	select {
	case err := <-ch:
		log.Infof("Stopping server: %v", err)
	}
}
