package main

import (
	"fmt"
	"os"
	"strconv"
	"strings"

	"github.com/u1f320/shorturl/db"
	"github.com/u1f320/shorturl/internal/mem"
	"github.com/u1f320/shorturl/internal/pg"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"

	_ "github.com/joho/godotenv/autoload"
)

var (
	storeType = os.Getenv("STORE_TYPE")
	storeDSN  = os.Getenv("STORE_DSN")
	port      = strings.Trim(os.Getenv("PORT"), ":")

	debug bool
)

func init() {
	debug, _ = strconv.ParseBool(os.Getenv("DEBUG"))
}

func main() {
	// set up a logger
	zcfg := zap.NewProductionConfig()
	zcfg.Encoding = "console"
	zcfg.EncoderConfig.EncodeTime = zapcore.ISO8601TimeEncoder
	zcfg.EncoderConfig.EncodeLevel = zapcore.CapitalColorLevelEncoder
	zcfg.EncoderConfig.EncodeCaller = zapcore.ShortCallerEncoder
	zcfg.EncoderConfig.EncodeDuration = zapcore.StringDurationEncoder

	if debug {
		zcfg.Level.SetLevel(zapcore.DebugLevel)
	} else {
		zcfg.Level.SetLevel(zapcore.InfoLevel)
	}

	z, err := zcfg.Build(zap.AddStacktrace(zapcore.ErrorLevel))
	if err != nil {
		panic(err)
	}
	zap.RedirectStdLog(z)
	log := z.Sugar()

	var db db.DB
	switch storeType {
	case PostgresStore:
		db, err = pg.New(storeDSN)
	case MemoryStore:
		db, err = mem.New()
	default:
	}

	if db == nil {
		printerr("No store set, shorturld cannot run.")
		if err != nil {
			printerr("The following error occurred while creating the store:\n%v", err)
		}
		os.Exit(1)
	}

	if len(os.Args) < 2 {
		printerr("No subcommand provided.")
		help()
		os.Exit(1)
	}

	switch os.Args[1] {
	case "serve":
		serverMain(db, log)
	case "createuser":
		if len(os.Args) < 3 {
			printerr("No username provided.")
			os.Exit(1)
		}

		u, token, err := db.CreateUser(os.Args[2])
		if err != nil {
			printerr("Couldn't create a user: %v", err)
			os.Exit(1)
		}
		printerr("Created a user!\nName: %v\nID: %v\nToken: %v\nBe careful, as the token is what allows you to create URLs.", u.Username, u.ID, token)
	case "resetuser":
		if len(os.Args) < 3 {
			printerr("No username provided.")
			os.Exit(1)
		}

		u, err := db.Username(os.Args[2])
		if err != nil {
			printerr("Couldn't find a user with that username.")
			os.Exit(1)
		}

		token, err := db.RegenerateToken(u.ID)
		if err != nil {
			printerr("Couldn't update %v's token: %v", u.Username, err)
			os.Exit(1)
		}

		printerr("Updated %v's token! Their new token is: %v", u.Username, token)
	case "deleteuser":
		if len(os.Args) < 3 {
			printerr("No username provided.")
			os.Exit(1)
		}

		u, err := db.Username(os.Args[2])
		if err != nil {
			printerr("Couldn't find a user with that username.")
			os.Exit(1)
		}

		fmt.Fprintf(os.Stderr, "Are you sure you want to delete %v? Type \"yes\" to confirm: ", u.Username)
		var in string
		_, err = fmt.Scanln(&in)
		if err != nil {
			log.Fatalf("Error scanning your input: %v", err)
		}

		if strings.TrimSpace(in) != "yes" {
			printerr("Cancelled.")
			os.Exit(1)
		}

		err = db.DeleteUser(u.ID)
		if err != nil {
			log.Fatalf("Error deleting user: %v")
		}

		printerr("User deleted!")
	case "help":
		help()
	default:
		printerr("Invalid subcommand (%v) provided.", os.Args[1])
		help()
		os.Exit(1)
	}
}

func help() {
	printerr("Valid subcommands:")
	printerr("  - help: show this help.")
	printerr("  - serve: serve shorturld on the port specified by the PORT environment variable.")
	printerr("  - createuser <name>: create a user and return their API token.")
	printerr("  - resetuser <name>: reset a user's API token and return it.")
	printerr("  - deleteuser <name>: delete a user and their links.")
}

// prints to os.Stderr
func printerr(tmpl string, args ...interface{}) {
	if !strings.HasSuffix(tmpl, "\n") {
		tmpl = tmpl + "\n"
	}

	fmt.Fprintf(os.Stderr, tmpl, args...)
}
