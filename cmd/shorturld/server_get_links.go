package main

import (
	"net/http"

	"emperror.dev/errors"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/render"
	"github.com/jackc/pgx/v4"
	"github.com/u1f320/shorturl/db"
)

func (s *server) getLinks(w http.ResponseWriter, r *http.Request) {
	urls, err := s.db.AllLinks()
	if err != nil {
		s.log.Errorf("Error getting links: %v", err)
		jsonError(w, r, http.StatusInternalServerError, "Internal server error")
		return
	}

	data := []linkData{}

	for _, u := range urls {
		data = append(data, toLinkData(u))
	}

	render.JSON(w, r, data)
}

func (s *server) getUserLinks(w http.ResponseWriter, r *http.Request) {
	u, err := s.db.Username(chi.URLParam(r, "user"))
	if err != nil {
		if errors.Cause(err) == pgx.ErrNoRows {
			jsonError(w, r, http.StatusNotFound, "Couldn't find that user")
			return
		}

		s.log.Errorf("Error getting user: %v", err)
		jsonError(w, r, http.StatusInternalServerError, "Internal server error")
		return
	}

	urls, err := s.db.UserLinks(u.ID)
	if err != nil {
		s.log.Errorf("Error getting links: %v", err)
		jsonError(w, r, http.StatusInternalServerError, "Internal server error")
		return
	}

	data := []linkData{}

	for _, u := range urls {
		data = append(data, toLinkData(u))
	}

	render.JSON(w, r, data)
}

func (s *server) getMyLinks(w http.ResponseWriter, r *http.Request) {
	u, ok := db.UserFromContext(r.Context())
	if !ok {
		jsonError(w, r, http.StatusBadRequest, "Somehow, you reached this point without authenticating")
		return
	}

	urls, err := s.db.UserLinks(u.ID)
	if err != nil {
		s.log.Errorf("Error getting links: %v", err)
		jsonError(w, r, http.StatusInternalServerError, "Internal server error")
		return
	}

	data := []linkData{}

	for _, u := range urls {
		data = append(data, toLinkData(u))
	}

	render.JSON(w, r, data)
}
