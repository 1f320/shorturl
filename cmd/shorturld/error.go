package main

import (
	"fmt"
	"net/http"

	"github.com/go-chi/render"
)

type apiError struct {
	Code  int    `json:"code"`
	Error string `json:"error"`
}

func jsonError(w http.ResponseWriter, r *http.Request, code int, err string) {
	render.Status(r, code)
	render.JSON(w, r, &apiError{
		Code:  code,
		Error: err,
	})
}

func jsonErrorf(w http.ResponseWriter, r *http.Request, code int, tmpl string, v ...interface{}) {
	render.Status(r, code)
	render.JSON(w, r, &apiError{
		Code:  code,
		Error: fmt.Sprintf(tmpl, v...),
	})
}
