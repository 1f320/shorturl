package main

import (
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/u1f320/shorturl/db"
	"go.uber.org/zap"
)

type server struct {
	mux *chi.Mux
	db  db.DB
	log *zap.SugaredLogger
}

func newServer(db db.DB, log *zap.SugaredLogger) *server {
	s := &server{
		mux: chi.NewMux(),
		db:  db,
		log: log,
	}

	s.mux.Use(middleware.CleanPath)
	s.mux.Use(middleware.Recoverer)

	// the actual short link redirect
	s.mux.Get(`/{link:\w+}`, s.getLink)
	// a "spy" page that shows the full link without redirecting
	s.mux.Get(`/{link:\w+}/info`, nil)
	s.mux.Get(`/{link:\w+}/info.json`, s.getLinkJSON)

	// return all links
	s.mux.Get("/links", s.getLinks)
	// get all links for a single user
	s.mux.Get(`/users/{user:[\w-]+}`, s.getUserLinks)

	s.mux.Route("/", func(r chi.Router) {
		r.Use(s.auth)

		// get your links
		r.Get("/users/@me", s.getMyLinks)

		// create a short link
		r.Post("/links", s.postLink)

		// delete a link
		r.Delete(`/links/{link:\w+}`, s.deleteLink)
	})

	s.mux.Get("/robots.txt", func(w http.ResponseWriter, _ *http.Request) {
		w.Write([]byte(`User-agent: *
Disallow: /`))
	})

	return s
}

func (s *server) auth(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		keys := r.Header["Authorization"]
		if len(keys) < 1 {
			jsonError(w, r, http.StatusUnauthorized, "Unauthorized")
			return
		}

		u, matched, err := s.db.ValidateToken(keys[0])
		if err != nil {
			s.log.Errorf("Error validating token: %v", err)
			jsonError(w, r, http.StatusInternalServerError, "Internal server error")
			return
		}

		if !matched {
			jsonError(w, r, http.StatusUnauthorized, "Unauthorized")
			return
		}

		ctx := db.NewUserContext(r.Context(), u)
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}
