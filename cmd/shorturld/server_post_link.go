package main

import (
	"net/http"
	"time"

	"github.com/go-chi/render"
	"github.com/google/uuid"
	"github.com/u1f320/shorturl/db"
)

type postLinkData struct {
	// The short name for this link. Randomly generated if empty.
	Name string `json:"name,omitempty"`
	// The URL this link points to.
	URL string `json:"url"`
	// The expiry for this link. Nil if it doesn't expire.
	Expires *time.Time `json:"expires,omitempty"`
}

type linkData struct {
	ID uuid.UUID `json:"id"`

	// The short URL
	Short string `json:"short"`
	// The full URL to redirect to
	URL string `json:"url"`

	// The user that created this short URL
	User *User `json:"user,omitempty"`

	// The time this URL expires and is removed from the database, nil if it doesn't expire.
	Expires *time.Time `json:"expires,omitempty"`
}

func toLinkData(u db.URL) linkData {
	data := linkData{
		ID:      u.ID,
		Short:   u.Short,
		URL:     u.URL,
		Expires: u.Expires,
	}

	if u.User.ID.String() != "" && u.User.Username != "" {
		data.User = &User{
			ID:       u.User.ID,
			Username: u.User.Username,
		}
	}

	return data
}

// User is a user.
type User struct {
	ID       uuid.UUID `json:"id"`
	Username string    `json:"username"`
}

func (s *server) postLink(w http.ResponseWriter, r *http.Request) {
	var link postLinkData

	err := render.Decode(r, &link)
	if err != nil {
		jsonError(w, r, http.StatusBadRequest, "Couldn't decode JSON data")
		return
	}

	u, ok := db.UserFromContext(r.Context())
	if !ok {
		jsonError(w, r, http.StatusInternalServerError, "You somehow reached this point without being authenticated")
		return
	}

	if link.URL == "" {
		jsonError(w, r, http.StatusBadRequest, "No URL to redirect to provided")
		return
	}

	var url db.URL
	if link.Name != "" {
		_, err = s.db.URL(link.Name)
		if err == nil {
			jsonError(w, r, http.StatusBadRequest, "A link with that name already exists")
			return
		}

		url, err = s.db.NamedURL(u.ID, link.Name, link.URL)
	} else {
		url, err = s.db.RandomURL(u.ID, link.URL)
	}
	if err != nil {
		s.log.Errorf("Couldn't create URL: %v", err)
		jsonError(w, r, http.StatusInternalServerError, "Couldn't create URL")
		return
	}

	render.JSON(w, r, toLinkData(url))
}
