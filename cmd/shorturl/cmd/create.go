package cmd

import (
	"fmt"

	"github.com/u1f320/shorturl/api"
	"github.com/urfave/cli/v2"
)

var Config = struct {
	Server, Token string
}{}

func Client() *api.Client {
	return api.NewClient(Config.Server, Config.Token, 0)
}

var Create = &cli.Command{
	Name:  "create",
	Usage: "Create a link",

	Flags: []cli.Flag{&cli.StringFlag{
		Name:    "name",
		Aliases: []string{"n"},
		Usage:   "Custom short name to use",
	}},
	Action: func(ctx *cli.Context) error {
		c := Client()

		name := ctx.String("name")

		link, err := c.CreateLinkComplex(api.CreateLinkData{
			Name: name,
			URL:  ctx.Args().First(),
		})
		if err != nil {
			return err
		}

		fmt.Printf("Link created!\n%v => %v\nID: %v\n", c.BaseURL+"/"+link.Short, link.URL, link.ID)
		return nil
	},
}
