create table if not exists users (
    id          uuid    primary key,
    username    text    not null    unique,
    token       text    not null
);

create index if not exists token_idx on users (token);

create table if not exists urls (
    id      uuid    primary key,
    short   text    not null    unique,
    url     text    not null,

    user_id uuid    not null    references users (id) on delete cascade,

    expires timestamp
);

create index if not exists short_url_idx on urls (short);

create or replace function generate_short() returns text as $$
    select string_agg(substr('abcdefghijklmnopqrstuvwxyz', ceil(random() * 26)::integer, 1), '') from generate_series(1, 5)
$$ language sql volatile;

create or replace function find_free_short_link() returns text as $$
declare new_short text;
begin
    loop
        new_short := generate_short();
        if not exists (select 1 from urls where short = new_short) then return new_short; end if;
    end loop;
end
$$ language plpgsql volatile;
