// Package pg implements db.DB with a PostgreSQL backend.
package pg

import (
	"context"
	"crypto/rand"
	"encoding/base64"
	"regexp"
	"time"

	"emperror.dev/errors"
	"github.com/georgysavva/scany/pgxscan"
	"github.com/google/uuid"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/u1f320/shorturl/db"

	// for schema
	_ "embed"
)

var tokenEncoding = base64.URLEncoding
var usernameRegex = regexp.MustCompile(`^[\w-]+$`)

//go:embed schema.sql
var schema string

type pg struct {
	*pgxpool.Pool
}

// New returns a new db.DB
func New(dsn string) (db.DB, error) {
	pool, err := pgxpool.Connect(context.Background(), dsn)
	if err != nil {
		return nil, err
	}

	pg := &pg{Pool: pool}

	err = pg.applyMigration()
	if err != nil {
		return nil, err
	}

	return pg, nil
}

func (pg *pg) applyMigration() error {
	_, err := pg.Exec(context.Background(), schema)
	return err
}

type dbURL struct {
	URL  URL     `db:"urls"`
	User db.User `db:"users"`
}

// URL is a single short URL.
type URL struct {
	ID      uuid.UUID  `db:"id"`
	Short   string     `db:"short"`
	URL     string     `db:"url"`
	Expires *time.Time `db:"expires"`
}

func (d dbURL) ToURL() db.URL {
	return db.URL{
		ID:      d.URL.ID,
		Short:   d.URL.Short,
		URL:     d.URL.URL,
		User:    d.User,
		Expires: d.URL.Expires,
	}
}

func (pg *pg) URL(short string) (url db.URL, err error) {
	var dbURL dbURL

	err = pg.QueryRow(context.Background(), "select users.id, users.username, urls.id, urls.short, urls.url, urls.expires from users, urls where urls.short = $1 and users.id = urls.user_id limit 1", short).Scan(&dbURL.User.ID, &dbURL.User.Username, &dbURL.URL.ID, &dbURL.URL.Short, &dbURL.URL.URL, &dbURL.URL.Expires)
	if err != nil {
		if errors.Cause(err) == pgx.ErrNoRows {
			return url, db.ErrNotFound
		}
		return
	}

	return dbURL.ToURL(), nil
}

func (pg *pg) RandomURL(userID uuid.UUID, url string) (u db.URL, err error) {
	var dbURL dbURL

	tx, err := pg.Begin(context.Background())
	if err != nil {
		return
	}

	err = pgxscan.Get(context.Background(), tx, &dbURL.URL, "insert into urls (id, user_id, short, url) values ($1, $2, find_free_short_link(), $3) returning id, short, url", uuid.New(), userID, url)
	if err != nil {
		tx.Rollback(context.Background())
		return
	}

	err = pgxscan.Get(context.Background(), tx, &dbURL.User, "select id, username from users where id = $1 limit 1", userID)
	if err != nil {
		tx.Rollback(context.Background())
		return
	}

	err = tx.Commit(context.Background())
	if err != nil {
		return
	}
	return dbURL.ToURL(), nil
}

func (pg *pg) CreateUser(name string) (u db.User, token string, err error) {
	conn, err := pg.Acquire(context.Background())
	if err != nil {
		return
	}

	exists := false
	err = conn.QueryRow(context.Background(), "select exists(select * from users where username = $1)", name).Scan(&exists)
	if err != nil || exists {
		if err == nil {
			err = db.ErrAlreadyExists
		}
		return
	}

	if !usernameRegex.MatchString(name) {
		return u, token, db.ErrInvalidUsername
	}

	id := uuid.New()

	bytes := make([]byte, 48)
	_, err = rand.Read(bytes)
	if err != nil {
		return
	}
	token = tokenEncoding.EncodeToString(bytes)

	err = pgxscan.Get(context.Background(), conn, &u, "insert into users (id, username, token) values ($1, $2, $3) returning id, username", id, name, token)
	if err != nil {
		return
	}

	return u, token, nil
}

// NamedURL creates a short URL with the given name
func (pg *pg) NamedURL(userID uuid.UUID, short, url string) (u db.URL, err error) {
	var dbURL dbURL

	tx, err := pg.Begin(context.Background())
	if err != nil {
		return
	}

	exists := false
	err = tx.QueryRow(context.Background(), "select exists(select * from urls where short = $1)", short).Scan(&exists)
	if err != nil || exists {
		if err == nil {
			err = db.ErrAlreadyExists
		}
		return
	}

	err = pgxscan.Get(context.Background(), tx, &dbURL.URL, "insert into urls (id, user_id, short, url) values ($1, $2, $3, $4) returning id, short, url", uuid.New(), userID, short, url)
	if err != nil {
		tx.Rollback(context.Background())
		return
	}

	err = pgxscan.Get(context.Background(), tx, &dbURL.User, "select id, username from users where id = $1 limit 1", userID)
	if err != nil {
		tx.Rollback(context.Background())
		return
	}

	err = tx.Commit(context.Background())
	if err != nil {
		return
	}
	return dbURL.ToURL(), nil
}

// User returns the user with the given UUID.
func (pg *pg) User(id uuid.UUID) (u db.User, err error) {
	err = pgxscan.Get(context.Background(), pg, &u, "select id, username from users where id = $1", id)
	if err != nil {
		if errors.Cause(err) == pgx.ErrNoRows {
			return u, db.ErrUserNotFound
		}
		return
	}
	return
}

// ValidateToken validates the given token
func (pg *pg) ValidateToken(token string) (u *db.User, valid bool, err error) {
	u = &db.User{}

	err = pgxscan.Get(context.Background(), pg, u, "select id, username from users where token = $1", token)
	if err != nil {
		if errors.Cause(err) == pgx.ErrNoRows {
			return nil, false, nil
		}
		return nil, false, err
	}

	return u, true, nil
}

// RegenerateToken generates a new token
func (pg *pg) RegenerateToken(id uuid.UUID) (token string, err error) {
	bytes := make([]byte, 48)
	_, err = rand.Read(bytes)
	if err != nil {
		return
	}
	token = tokenEncoding.EncodeToString(bytes)

	ct, err := pg.Exec(context.Background(), "update users set token = $1 where id = $2", token, id)
	if err != nil {
		return "", err
	}
	if ct.RowsAffected() == 0 {
		return "", errors.Sentinel("no rows affected")
	}
	return token, nil
}

func (pg *pg) Username(name string) (u db.User, err error) {
	err = pgxscan.Get(context.Background(), pg, &u, "select id, username from users where username = $1", name)
	if err != nil {
		if errors.Cause(err) == pgx.ErrNoRows {
			return u, db.ErrUserNotFound
		}
		return
	}
	return
}

func (pg *pg) AllLinks() (u []db.URL, err error) {
	err = pgxscan.Select(context.Background(), pg, &u, "select id, short, url, expires from urls")
	return
}

func (pg *pg) UserLinks(id uuid.UUID) (u []db.URL, err error) {
	err = pgxscan.Select(context.Background(), pg, &u, "select id, short, url, expires from urls where user_id = $1", id)
	return
}

func (pg *pg) DeleteURL(short string) (err error) {
	_, err = pg.Exec(context.Background(), "delete from urls where short = $1", short)
	return
}

func (pg *pg) DeleteUser(id uuid.UUID) (err error) {
	_, err = pg.Exec(context.Background(), "delete from users where id = $1", id)
	return
}
